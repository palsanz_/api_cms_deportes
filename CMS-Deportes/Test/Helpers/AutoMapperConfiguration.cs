﻿using API.Assets;
using AutoMapper;

namespace Test.Helpers
{
    public static class AutoMapperConfiguration
    {
        public static IMapper GetImapper()
        {
            MappingProfile myProfile = new MappingProfile();
            MapperConfiguration configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            return new Mapper(configuration);
        }
    }
}