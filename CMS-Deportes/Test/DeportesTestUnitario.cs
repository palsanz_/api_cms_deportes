using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using EFCoreDatabase.Context;
using Xunit;
using EFCoreDatabase.Models;
using EFCoreDatabase.Repositories;
using EFCoreDatabase.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Service.Services;
using Service.Services.Interfaces;
using Service.ViewModels;
using Test.Helpers;
using TestSupport.EfHelpers;
using FluentAssertions;

namespace Test
{
    public class DeportesTestUnitario
    {

        private readonly IMapper _mapper = null;

        public DeportesTestUnitario()
        {
            _mapper = AutoMapperConfiguration.GetImapper();
        }


        [Fact]
        public async Task GetDeporte_Deporte1_Returns1Deporte()
        {
            var data = GetDeportesTest();
            DbContextOptions<EurovalDbContext> options = SqliteInMemory.CreateOptions<EurovalDbContext>();

            using (var context = new EurovalDbContext(options))
            {
                await SeedContextHelper.SeedRangeDatabaseSameContext(context, data);
                var deporteService = GetDeporteService(context);
                List<VMDeporte> deportes = await deporteService.Get();

                //Assert
                deportes[0].Nombre.Should().Be("F�tbol");
            }
        }


        private List<Deporte> GetDeportesTest()
        {
            var data = new List<Deporte>
            {
                new Deporte{Nombre= "F�tbol"},
                new Deporte{Nombre= "Paddle"},
                new Deporte{Nombre= "Tenis"},
                new Deporte{Nombre= "Squash"},
            };

            return data;
        }


        private IDeporteService GetDeporteService(EurovalDbContext context)
        {
            IDeporteRepository repository = new DeporteRepository(context);
            IDeporteService service = new DeporteService(repository,_mapper);
            return service;
        }
    }
}
