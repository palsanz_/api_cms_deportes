﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using EFCoreDatabase.Models;
using EFCoreDatabase.Repositories;
using EFCoreDatabase.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Service.Services.Interfaces;
using Service.ViewModels;

namespace Service.Services
{
    public class PistaService : IPistaService
    {
        private readonly IMapper _mapper;
        private readonly IDeporteRepository _deporteRepository;
        private readonly IPistaRepository _pistaRepository;
        private readonly ISocioRepository _socioRepository;

        public PistaService(IDeporteRepository deporteRepository, IPistaRepository pistaRepository, ISocioRepository socioRepository, IMapper mapper)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _deporteRepository = deporteRepository ?? throw new ArgumentNullException(nameof(DeporteRepository));
            _pistaRepository = pistaRepository ?? throw new ArgumentNullException(nameof(PistaRepository));
            _socioRepository = socioRepository ?? throw new ArgumentNullException(nameof(SocioRepository));

        }

        public async Task<List<VMPistaDetalle>> Get()
        {
            var pistas = await _pistaRepository.Get()
                .ProjectTo<VMPistaDetalle>(_mapper.ConfigurationProvider)
                .ToListAsync();

            return pistas;
        }

        public async Task<List<VMPistaDetalle>> GetDisponiblesByFilter(DateTime fecha, int deporteId, long socioId)
        {
            List<VMPistaDetalle> pistas = null;
            //Comprobamos que el deporte exista.
            var deporte = await _deporteRepository.GetById(deporteId).FirstOrDefaultAsync();
            //Comprobamos que exista el socio
            //var socio = await _socioRepository.GetById(socioId).FirstOrDefaultAsync();

            if (deporte != null)
                pistas = await _pistaRepository.GetDisponiblesByFilter(fecha, deporteId, socioId)
                    .ProjectTo<VMPistaDetalle>(_mapper.ConfigurationProvider)
                    .ToListAsync();

            return pistas;

        }

        public async Task<VMPistaDetalle> GetById(int id)
        {
            var pista = await _pistaRepository.GetById(id)
                .ProjectTo<VMPistaDetalle>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();

            return pista;
        }

        public async Task<VMPista> Add(VMPista vmPista)
        {
            VMPista result = null;

            //Comprobamos que el deporte exista.
            var deporte = await _deporteRepository.GetById(vmPista.DeporteId).FirstOrDefaultAsync();

            if (deporte != null)
            {
                var pista = new Pista
                {
                    DeporteId = vmPista.DeporteId,
                    Nombre = vmPista.Nombre,
                    Descripcion = vmPista.Descripcion
                };

                await _pistaRepository.Add(pista);
                result = _mapper.Map<VMPista>(pista);

            }

            return result;
        }

        public async Task<VMPista> Update(VMPista vmPista)
        {

            var pistaDetalle = await _pistaRepository.GetById(vmPista.Id)
                .ProjectTo<VMPistaDetalle>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
            
            var deporte = await _deporteRepository.GetById(vmPista.DeporteId).FirstOrDefaultAsync();

            if (pistaDetalle != null && deporte != null)
            {
                var pista = new Pista
                {
                    DeporteId = vmPista.DeporteId,
                    Nombre = vmPista.Nombre,
                    Descripcion = vmPista.Descripcion
                };
               
                await _pistaRepository.Update(pista);
            }

            return pistaDetalle;
        }

        public async Task<bool> Delete(int id)
        {
            var pista = await _pistaRepository.GetById(id).FirstOrDefaultAsync();

            if (pista != null)
            {
                await _pistaRepository.Delete(pista);
                return true;
            }

            return false;
        }
    }
}