﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using EFCoreDatabase.Models;
using EFCoreDatabase.Repositories;
using EFCoreDatabase.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Service.Services.Interfaces;
using Service.ViewModels;

namespace Service.Services
{
    public class ReservaService : IReservaService
    {
        private readonly IMapper _mapper;
        private readonly IPistaRepository _pistaRepository;
        private readonly IReservaRepository _reservaRepository;
        private readonly ISocioRepository _socioRepository;

        public ReservaService(IReservaRepository reservaRepository, IPistaRepository pistaRepository, ISocioRepository socioRepository, IMapper mapper)
        {
            _reservaRepository = reservaRepository ?? throw new ArgumentNullException(nameof(ReservaRepository));
            _pistaRepository = pistaRepository ?? throw new ArgumentNullException(nameof(PistaRepository));
            _socioRepository = socioRepository ?? throw new ArgumentNullException(nameof(SocioRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<VMReservaDetalle> GetById(long id)
        {
            var reserva = await _reservaRepository.GetById(id)
                .ProjectTo<VMReservaDetalle>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();

            return reserva;
        }

        public async Task<List<VMReservaDetalle>> Get()
        {
            var reservas = await _reservaRepository.Get()
                .ProjectTo<VMReservaDetalle>(_mapper.ConfigurationProvider)
                .ToListAsync();
            return reservas;
        }

        public async Task<List<VMReservaDetalle>> GetByDate(DateTime fecha)
        {
            var reservas = await _reservaRepository.GetByDate(fecha)
                .ProjectTo<VMReservaDetalle>(_mapper.ConfigurationProvider)
                .ToListAsync();

            return reservas;
        }

        public async Task<VMReserva> Add(VMReserva vmReserva)
        {
            VMReserva vmResult = null;
            var pista = await _pistaRepository.GetById(vmReserva.PistaId).FirstOrDefaultAsync();
            var socio = await _socioRepository.GetById(vmReserva.SocioId).FirstOrDefaultAsync();

            if (pista != null && 
                socio != null &&
                HoraReservaOk(vmReserva.Fecha) &&
                PistaDisponible(vmReserva.Fecha, pista) && 
                SocioPuedeReservar(vmReserva.Fecha, socio))
            {
                var reserva = new Reserva
                {
                    Fecha = vmReserva.Fecha,
                    SocioId = vmReserva.SocioId,
                    PistaId = vmReserva.PistaId,
                    FechaCreacion = DateTime.Now,
                    FechaActualizacion = DateTime.Now,
                    UsuarioId = vmReserva.UsuarioId
                };

                await _reservaRepository.Add(reserva);
                vmResult = _mapper.Map<VMReserva>(reserva);
            }

            return vmResult;
        }

        public async Task<VMReserva> Update(VMReserva vmReserva)
        {
            var reserva = await _reservaRepository.GetById(vmReserva.Id).FirstOrDefaultAsync();

            if (reserva != null)
            {
                var pista = await _pistaRepository.GetById(vmReserva.PistaId).FirstOrDefaultAsync();
                var socio = await _socioRepository.GetById(vmReserva.SocioId).FirstOrDefaultAsync();

                if (pista != null && 
                    socio != null &&
                    HoraReservaOk(vmReserva.Fecha) &&
                    PistaDisponible(vmReserva.Fecha, pista))
                {
                    reserva.Fecha = vmReserva.Fecha;
                    reserva.PistaId = vmReserva.PistaId;
                    reserva.FechaActualizacion = DateTime.Now;
                    reserva.UsuarioId = vmReserva.UsuarioId;

                    await _reservaRepository.Update(reserva);
                }
            }

            return vmReserva;
        }

        public async Task<bool> Delete(long id)
        {
            var reserva = await _reservaRepository.GetById(id).FirstOrDefaultAsync();

            if (reserva != null)
            {
                await _reservaRepository.Delete(reserva);
                return true;
            }

            return false;
        }


        /// <summary>
        /// Comprobamos que la fecha de la reserva esté dentro del rango definido
        /// </summary>
        /// <param name="fecha"></param>
        /// <returns></returns>
        private Boolean HoraReservaOk(DateTime fecha)
        {
            int horaInicio = 8;
            int horaFin = 22;

            if (fecha.Hour < horaInicio || fecha.Hour > horaFin || fecha.Minute != 0)
                return false;

            return true;
        }

        /// <summary>
        /// Comprueba si la pista está disponible
        /// </summary>
        /// <param name="fecha"></param>
        /// <param name="pista"></param>
        /// <returns></returns>
        private Boolean PistaDisponible(DateTime fecha, Pista pista)
        {
            IQueryable<Pista> disponibles = _pistaRepository.GetDisponiblesByFilter(fecha: fecha, deporte: pista.DeporteId, socio: null);
            return disponibles.Any(m => m.Id == pista.Id);

        }

        private Boolean SocioPuedeReservar(DateTime fecha, Socio socio)
        {
            return true;
        }

    }
}