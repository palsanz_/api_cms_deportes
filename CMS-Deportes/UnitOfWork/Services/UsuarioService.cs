﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using EFCoreDatabase.Models;
using EFCoreDatabase.Repositories;
using EFCoreDatabase.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Service.Services.Interfaces;
using Service.ViewModels;

namespace Service.Services
{
    public class UsuarioService : IUsuarioService
    {
        private readonly IUsuarioRepository _usuarioRepository;
        private readonly IMapper _mapper;

        public UsuarioService(IMapper mapper, IUsuarioRepository usuarioRepository)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _usuarioRepository = usuarioRepository ?? throw new ArgumentNullException(nameof(UsuarioRepository));
        }

        public async Task<List<VMUsuario>> Get()
        {
            var usuarios = await _usuarioRepository.Get()
                .ProjectTo<VMUsuario>(_mapper.ConfigurationProvider)
                .ToListAsync();

            return usuarios;
        }

        public async Task<VMUsuario> GetById(int id)
        {
            var usuario = await _usuarioRepository.GetById(id).Where(m => m.Borrado == false)
                .ProjectTo<VMUsuario>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();

            return usuario;
        }

        public async Task<VMUsuario> GetByUserName(string userName)
        {
            var usuario = await _usuarioRepository.GetByUserName(userName)
                .Where(m => m.Borrado == false)
                .ProjectTo<VMUsuario>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();

            return usuario;
        }

        public async Task<List<VMUsuario>> GetByName(string name)
        {
            var usuarios = await _usuarioRepository.GetByName(name)
                .Where(m => m.Borrado == false)
                .ProjectTo<VMUsuario>(_mapper.ConfigurationProvider)
                .ToListAsync();


            return usuarios;
        }

        public async Task<VMUsuario> Add(VMUsuario vmUsuario)
        {
            VMUsuario vmResult = null;
            var socio = new Usuario
            {
                Usuario1 = vmUsuario.Usuario1,
                Nombre = vmUsuario.Nombre,
                Apellido1 = vmUsuario.Apellido1,
                Apellido2 = vmUsuario.Apellido2,
                Email = vmUsuario.Email,
                Activo = true,
                Borrado = false
            };

            await _usuarioRepository.Add(socio);

             vmResult = _mapper.Map<VMUsuario>(socio);
           

            return vmResult;
        }

        public async Task<VMUsuario> Update(VMUsuario vmUsuario)
        {
            var usuario = await _usuarioRepository.GetById(vmUsuario.Id).FirstOrDefaultAsync();

            //Si el usuario existe y no está borrado lo actualizamos.
            if (usuario != null && usuario.Borrado == false)
            {
                usuario.Usuario1 = vmUsuario.Usuario1;
                usuario.Nombre = vmUsuario.Nombre;
                usuario.Apellido1 = vmUsuario.Apellido1;
                usuario.Apellido2 = vmUsuario.Apellido2;
                usuario.Email = vmUsuario.Email;
                usuario.Activo = vmUsuario.Activo;
                await _usuarioRepository.Update(usuario);
            }

            return vmUsuario;
        }

        public async Task<bool> Delete(int id)
        {
            var usuario = await _usuarioRepository.GetById(id).FirstOrDefaultAsync();

            if (usuario != null)
            {
                await _usuarioRepository.Delete(usuario);
                return true;
            }

            return false;
        }
    }
}