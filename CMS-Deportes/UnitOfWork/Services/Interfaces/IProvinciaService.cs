﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Service.ViewModels;

namespace Service.Services
{
    public interface IProvinciaService
    {
        Task<List<VMProvincia>> Get();
        Task<VMProvincia> GetById(int id);
    }
}