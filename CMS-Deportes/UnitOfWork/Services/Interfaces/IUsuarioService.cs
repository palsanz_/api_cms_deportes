﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EFCoreDatabase.Models;
using Service.ViewModels;

namespace Service.Services.Interfaces
{
    /// <summary>
    /// Interfaz de la clase UsuarioService
    /// </summary>
    public interface IUsuarioService
    {
        /// <summary>
        /// Obtenemos un listado de usuarios
        /// </summary>
        /// <returns></returns>
        Task<List<VMUsuario>> Get();

        /// <summary>
        /// Obtenemos un usuario por Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<VMUsuario> GetById(int id);

        /// <summary>
        /// Obtenemos un usuario por nombre de usuario
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        Task<VMUsuario> GetByUserName(string userName);

        /// <summary>
        /// Obtenemos un listado de usuarios por nombre de pila
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        Task<List<VMUsuario>> GetByName(string name);

        /// <summary>
        /// Agregamos un usuario
        /// </summary>
        /// <param name="vmUsuario"></param>
        /// <returns></returns>
        Task<VMUsuario> Add(VMUsuario vmUsuario);

        /// <summary>
        /// Actualizamos un usuario
        /// </summary>
        /// <param name="vmUsuario"></param>
        /// <returns></returns>
        Task<VMUsuario> Update(VMUsuario vmUsuario);

        /// <summary>
        /// Borrado de un usuario
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<bool> Delete(int id);
    }
}