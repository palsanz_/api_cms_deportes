﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EFCoreDatabase.Models;
using Service.ViewModels;

namespace Service.Services.Interfaces
{
    public interface IPistaService
    {
        /// <summary>
        /// Obtenemos las pistas disponibles en base a los filtros pasado
        /// </summary>
        /// <param name="fecha"></param>
        /// <param name="deporteId"></param>
        /// <param name="socioId"></param>
        /// <returns></returns>
        Task<List<VMPistaDetalle>> GetDisponiblesByFilter(DateTime fecha, int deporteId, long socioId);

        /// <summary>
        /// Obtenemos un listado de pistas.
        /// </summary>
        /// <returns></returns>
        Task<List<VMPistaDetalle>> Get();

        /// <summary>
        /// Obtenemos una pista en base a un Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<VMPistaDetalle> GetById(int id);

        /// <summary>
        /// Agregamos una pista
        /// </summary>
        /// <param name="vmPista"></param>
        /// <returns></returns>
        Task<VMPista> Add(VMPista vmPista);

        /// <summary>
        /// Actualizamos una pista
        /// </summary>
        /// <param name="vmPista"></param>
        /// <returns></returns>
        Task<VMPista> Update(VMPista vmPista);

        /// <summary>
        /// Borrado de una pista.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<bool> Delete(int id);
    }
}