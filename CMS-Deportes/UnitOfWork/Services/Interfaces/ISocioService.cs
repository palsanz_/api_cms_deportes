﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EFCoreDatabase.Models;
using Service.ViewModels;

namespace Service.Services.Interfaces
{
    /// <summary>
    /// Interfaz de Socios
    /// </summary>
    public interface ISocioService
    {
        /// <summary>
        /// Obtenemos un listado de socios completo
        /// </summary>
        /// <returns></returns>
        Task<List<VMSocioDetalle>> Get();

        /// <summary>
        /// Obtenemos un socio por Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<VMSocioDetalle> GetById(long id);

        /// <summary>
        /// Obtenemos socios por nombre
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        Task<List<VMSocioDetalle>> GetByName(string name);

        /// <summary>
        /// Agregamos un socio.
        /// </summary>
        /// <param name="vmSocio"></param>
        /// <returns></returns>
        Task<VMSocio> Add(VMSocio vmSocio);

        /// <summary>
        /// Actualizamos un socio
        /// </summary>
        /// <param name="vmSocio"></param>
        /// <returns></returns> 
        Task<VMSocio> Update(VMSocio vmSocio);

        /// <summary>
        /// Borramos un socio.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<bool> Delete(long id);
    }
}