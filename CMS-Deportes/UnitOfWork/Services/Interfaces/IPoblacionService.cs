﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Service.ViewModels;

namespace Service.Services
{
    public interface IPoblacionService
    {
        Task<List<VMPoblacion>> Get();
        Task<List<VMPoblacion>> GetByProvincia(int provincia);
        Task<VMPoblacion> GetById(int id);
    }
}