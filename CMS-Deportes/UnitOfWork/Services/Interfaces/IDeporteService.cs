﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Service.ViewModels;

namespace Service.Services.Interfaces
{
    /// <summary>
    /// Interfaz del servicio Deporte
    /// </summary>
    public interface IDeporteService
    {
        /// <summary>
        /// Obtenemos un listado de deportes disponibles
        /// </summary>
        /// <returns></returns>
        Task<List<VMDeporte>> Get();

        /// <summary>
        /// Obtenemos un deporte por su Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<VMDeporte> GetById(int id);

        /// <summary>
        /// Agregamos un deporte
        /// </summary>
        /// <param name="vmDeporte"></param>
        /// <returns></returns>
        Task<VMDeporte> Add(VMDeporte vmDeporte);
        
        /// <summary>
        /// Actualizamos un deporte
        /// </summary>
        /// <param name="vmDeporte"></param>
        /// <returns></returns>
        Task<VMDeporte> Update(VMDeporte vmDeporte);

        /// <summary>
        /// Borramos un deporte
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<bool> Delete(int id);
    }
}