﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EFCoreDatabase.Models;
using Service.ViewModels;

namespace Service.Services.Interfaces
{
    /// <summary>
    /// Interfaz de Reservas
    /// </summary>
    public interface IReservaService
    {
        /// <summary>
        /// Obtenemos un listado de reservas
        /// </summary>
        /// <returns></returns>
        Task<List<VMReservaDetalle>> Get();

        /// <summary>
        /// Obtenemos una reserva por identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<VMReservaDetalle> GetById(long id);

        /// <summary>
        /// Obtenemos una listado de reservas por fecha
        /// </summary>
        /// <param name="fecha"></param>
        /// <returns></returns>
        Task<List<VMReservaDetalle>> GetByDate(DateTime fecha);

        /// <summary>
        /// Registramos una reserva
        /// </summary>
        /// <param name="vmReserva"></param>
        /// <returns></returns>
        Task<VMReserva> Add(VMReserva vmReserva);

        /// <summary>
        /// Actualizamos una reserva
        /// </summary>
        /// <param name="vmReserva"></param>
        /// <returns></returns>
        Task<VMReserva> Update(VMReserva vmReserva);

        /// <summary>
        /// Borrado de una reserva
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<bool> Delete(long id);
    }
}