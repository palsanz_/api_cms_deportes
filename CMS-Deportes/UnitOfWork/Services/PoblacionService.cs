﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using EFCoreDatabase.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Service.ViewModels;

namespace Service.Services
{
    public class PoblacionService : IPoblacionService
    {
        private readonly IMapper _mapper;
        private readonly IPoblacionRepository _poblacionRepository;

        public PoblacionService(IPoblacionRepository poblacionRepository, IMapper mapper)
        {
            _poblacionRepository = poblacionRepository ?? throw new ArgumentNullException(nameof(poblacionRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<List<VMPoblacion>> Get()
        {
            var poblaciones = await _poblacionRepository.Get()
                .ProjectTo<VMPoblacion>(_mapper.ConfigurationProvider)
                .ToListAsync();

            return poblaciones;
        }

        public async Task<List<VMPoblacion>> GetByProvincia(int provincia)
        {
            var poblaciones = await _poblacionRepository.GetByProvincia(provincia)
                .ProjectTo<VMPoblacion>(_mapper.ConfigurationProvider)
                .ToListAsync();

            return poblaciones;
        }

        public async Task<VMPoblacion> GetById(int id)
        {
            var deporte = await _poblacionRepository.GetById(id)
                .ProjectTo<VMPoblacion>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();

            return deporte;
        }
    }
}