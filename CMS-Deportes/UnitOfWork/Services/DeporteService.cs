﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using EFCoreDatabase.Models;
using EFCoreDatabase.Repositories;
using EFCoreDatabase.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Service.Services.Interfaces;
using Service.ViewModels;

namespace Service.Services
{

    public class DeporteService : IDeporteService
    {
        private readonly IMapper _mapper;
        private readonly IDeporteRepository _deporteRepository;

        public DeporteService(IDeporteRepository deporteRepository, IMapper mapper)
        {
            _deporteRepository = deporteRepository ?? throw new ArgumentNullException(nameof(DeporteRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<List<VMDeporte>> Get()
        {
            var deportes = await _deporteRepository.Get()
                .ProjectTo<VMDeporte>(_mapper.ConfigurationProvider)
                .ToListAsync();

            return deportes;
        }

        public async Task<VMDeporte> GetById(int id)
        {
            var deporte = await _deporteRepository.GetById(id)
                .ProjectTo<VMDeporte>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();

            return deporte;
        }

        public async Task<VMDeporte> Add(VMDeporte vmDeporte)
        {
            var deporte = new Deporte { Nombre = vmDeporte.Nombre };
            await _deporteRepository.Add(deporte);
            var result = _mapper.Map<VMDeporte>(deporte);
            return result;
        }

        public async Task<VMDeporte> Update(VMDeporte vmDeporte)
        {
            var deporte = await _deporteRepository.GetById(vmDeporte.Id).FirstOrDefaultAsync();

            if (deporte != null)
            {
                deporte.Nombre = vmDeporte.Nombre;
                await _deporteRepository.Update(deporte);
            }

            return vmDeporte;
        }

        public async Task<bool> Delete(int id)
        {
            var deporte = await _deporteRepository.GetById(id).FirstOrDefaultAsync();

            if (deporte != null)
            {
                await _deporteRepository.Delete(deporte);
                return true;
            }

            return false;
        }
    }


}