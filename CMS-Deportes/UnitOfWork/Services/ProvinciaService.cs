﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using EFCoreDatabase.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Service.ViewModels;

namespace Service.Services
{
    public class ProvinciaService : IProvinciaService
    {
        private readonly IMapper _mapper;
        private readonly IProvinciaRepository _provinciaRepository;

        public ProvinciaService(IProvinciaRepository provinciaRepository, IMapper mapper)
        {
            _provinciaRepository = provinciaRepository ?? throw new ArgumentNullException(nameof(provinciaRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<List<VMProvincia>> Get()
        {
            var poblaciones = await _provinciaRepository.Get()
                .ProjectTo<VMProvincia>(_mapper.ConfigurationProvider)
                .ToListAsync();

            return poblaciones;
        }
       

        public async Task<VMProvincia> GetById(int id)
        {
            var deporte = await _provinciaRepository.GetById(id)
                .ProjectTo<VMProvincia>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();

            return deporte;
        }
    }
}