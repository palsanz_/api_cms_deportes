﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using EFCoreDatabase.Models;
using EFCoreDatabase.Repositories;
using EFCoreDatabase.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Service.Services.Interfaces;
using Service.ViewModels;

namespace Service.Services
{
    public class SocioService : ISocioService
    {
        private readonly ISocioRepository _socioRepository;
        private readonly IMapper _mapper;

        public SocioService(ISocioRepository socioRepository, IMapper mapper)
        {
            _socioRepository = socioRepository ?? throw new ArgumentNullException(nameof(SocioRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<List<VMSocioDetalle>> Get()
        {
            var socios = await _socioRepository.Get()
                .ProjectTo<VMSocioDetalle>(_mapper.ConfigurationProvider)
                .ToListAsync();

            return socios;
        }

        public async Task<VMSocioDetalle> GetById(long id)
        {
            var socio = await _socioRepository.GetById(id)
                                              .Where(m => m.Borrado == false)
                                              .ProjectTo<VMSocioDetalle>(_mapper.ConfigurationProvider)
                                              .FirstOrDefaultAsync();

            return socio;
        }

        public async Task<List<VMSocioDetalle>> GetByName(string name)
        {
            var socios = await _socioRepository.GetByName(name)
                                               .Where(m => m.Borrado == false)
                                               .ProjectTo<VMSocioDetalle>(_mapper.ConfigurationProvider)
                                               .ToListAsync();
            return socios;
        }

        public async Task<VMSocio> Add(VMSocio vmSocio)
        {
            VMSocio vmResult = null;
            var socio = new Socio
            {
                Nombre = vmSocio.Nombre,
                Apellido1 = vmSocio.Apellido1,
                Apellido2 = vmSocio.Apellido2,
                Email = vmSocio.Email,
                FechaNacimiento = vmSocio.FechaNacimiento,
                Direccion = vmSocio.Direccion,
                PoblacionId = vmSocio.PoblacionId,
                CodigoPostal = vmSocio.CodigoPostal,
                UsuarioId = vmSocio.UsuarioId,
                Borrado = false
            };

            await _socioRepository.Add(socio);
            vmResult = _mapper.Map<VMSocio>(socio);

            return vmResult;
        }

        public async Task<VMSocio> Update(VMSocio vmSocio)
        {
            var socio = await _socioRepository.GetById(vmSocio.Id)
                                              .FirstOrDefaultAsync();

            //Si el socio existe y no está borrado lo actualizamos.
            if (socio != null && socio.Borrado == false)
            {
                socio.Nombre = vmSocio.Nombre;
                socio.Apellido1 = vmSocio.Apellido1;
                socio.Apellido2 = vmSocio.Apellido2;
                socio.Email = vmSocio.Email;
                socio.FechaNacimiento = vmSocio.FechaNacimiento;
                socio.Direccion = vmSocio.Direccion;
                socio.PoblacionId = vmSocio.PoblacionId;
                socio.CodigoPostal = vmSocio.CodigoPostal;
                socio.UsuarioId = vmSocio.UsuarioId;
                await _socioRepository.Update(socio);
            }

            return vmSocio;
        }

        public async Task<bool> Delete(long id)
        {
            var socio = await _socioRepository.GetById(id).FirstOrDefaultAsync();

            if (socio != null)
            {
                await _socioRepository.Delete(socio);
                return true;
            }

            return false;
        }
    }
}