﻿using System.ComponentModel.DataAnnotations;

namespace Service.ViewModels
{
    public class VMUsuario
    {
        public int Id { get; set; }

        [Required]
        [StringLength(20)]
        public string Usuario1 { get; set; }

        [Required]
        [StringLength(100)]
        public string Nombre { get; set; }

        [Required]
        [StringLength(50)]
        public string Apellido1 { get; set; }

        [Required]
        [StringLength(50)]
        public string Apellido2 { get; set; }

        [Required]
        [StringLength(50)]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public bool? Activo { get; set; }
        
    }
}