﻿using System.ComponentModel.DataAnnotations;

namespace Service.ViewModels
{
    public class VMDeporte
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Nombre { get; set; }
    }
}