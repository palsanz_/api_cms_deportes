﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Service.ViewModels
{
    public class VMSocio
    {
        public long Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Nombre { get; set; }

        [Required]
        [StringLength(50)]
        public string Apellido1 { get; set; }

        [Required]
        [StringLength(50)]
        public string Apellido2 { get; set; }

        [Required]
        [StringLength(100)]
        [EmailAddress]
        public string Email { get; set; }
        
        [DataType(DataType.Date)]
        public DateTime? FechaNacimiento { get; set; }
        
        [StringLength(500)]
        public string Direccion { get; set; }

        public int? PoblacionId { get; set; }

        [StringLength(10)]
        public string CodigoPostal { get; set; }

        [Required]
        public int UsuarioId { get; set; }
    }
}