﻿using System;

namespace Service.ViewModels
{
    public class VMReservaDetalle :VMReserva
    {
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaActualizacion { get; set; }
        public VMPista Pista { get; set; }
        public VMSocio Socio { get; set; }
        public VMUsuario Usuario { get; set; }
    }
}