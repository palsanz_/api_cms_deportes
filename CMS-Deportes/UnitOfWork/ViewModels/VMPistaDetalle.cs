﻿using System.ComponentModel.DataAnnotations;

namespace Service.ViewModels
{
    public class VMPistaDetalle:VMPista
    {
        public int Id { get; set; }
       
        public VMDeporte Deporte { get; set; }
    }
}