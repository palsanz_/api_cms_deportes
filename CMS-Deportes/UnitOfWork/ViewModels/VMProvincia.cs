﻿using System.ComponentModel.DataAnnotations;

namespace Service.ViewModels
{
    public class VMProvincia
    {
        public int Id { get; set; }

        [Required]
        [StringLength(10)]
        public string Cod { get; set; }

        [Required]
        [StringLength(100)]
        public string Nombre { get; set; }
    }
}