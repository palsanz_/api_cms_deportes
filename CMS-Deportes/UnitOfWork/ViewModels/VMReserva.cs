﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Service.ViewModels
{
    public class VMReserva
    {
        public long Id { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime Fecha { get; set; }

        [Required]
        public long SocioId { get; set; }

        [Required]
        public int PistaId { get; set; }

        [Required]
        public int UsuarioId { get; set; }
    }
}