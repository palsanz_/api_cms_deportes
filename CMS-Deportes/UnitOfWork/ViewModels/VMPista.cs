﻿using System.ComponentModel.DataAnnotations;

namespace Service.ViewModels
{
    public class VMPista
    {
        public int Id { get; set; }

        [Required]
        public int DeporteId { get; set; }

        [Required]
        [StringLength(50)]
        public string Nombre { get; set; }

        [StringLength(200)]
        public string Descripcion { get; set; }
       
    }
}