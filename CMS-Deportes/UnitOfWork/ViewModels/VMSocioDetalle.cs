﻿using System;

namespace Service.ViewModels
{
    public class VMSocioDetalle :VMSocio
    {
        public DateTime FechaAlta { get; set; }
        public DateTime? FechaActualizacion { get; set; }
        public bool Borrado { get; set; }

        public VMPoblacion Poblacion { get; set; }
        public VMUsuario Usuario { get; set; }
    }
}