﻿using System.ComponentModel.DataAnnotations;

namespace Service.ViewModels
{
    public class VMPoblacion
    {
        public int Id { get; set; }

        [Required]
        public int ProvinciaId { get; set; }

        [StringLength(200)]
        public string Nombre { get; set; }

        public VMProvincia Provincia { get; set; }

    }
}