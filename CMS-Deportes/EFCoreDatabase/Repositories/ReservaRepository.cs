﻿using System;
using System.Linq;
using System.Threading.Tasks;
using EFCoreDatabase.Context;
using EFCoreDatabase.Models;
using EFCoreDatabase.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace EFCoreDatabase.Repositories
{
    public class ReservaRepository : IReservaRepository
    {
        private readonly EurovalDbContext _context;

        public ReservaRepository(EurovalDbContext context)
        {
            _context = context;
        }

        public IQueryable<Reserva> Get()
        {
            return _context.Reserva.AsNoTracking();
        }

        public IQueryable<Reserva> GetById(long id)
        {
            return _context.Reserva.Where(m => m.Id == id);
        }

        public IQueryable<Reserva> GetByDate(DateTime fecha)
        {
            return _context.Reserva
                .Where(m => m.Fecha.Day == fecha.Day && m.Fecha.Month == fecha.Month && m.Fecha.Year == fecha.Year)
                .AsNoTracking();
        }

        public async Task<Reserva> Add(Reserva reserva)
        {
            if (reserva != null)
            {
                await _context.AddAsync(reserva);
                await _context.SaveChangesAsync();
            }

            return reserva;
        }

        public async Task Update(Reserva reserva)
        {
            if (reserva != null)
            {
                _context.Reserva.Update(reserva);
                await _context.SaveChangesAsync();
            }
        }

        public async Task Delete(Reserva reserva)
        {
            if (reserva != null)
            {
                _context.Reserva.Remove(reserva);
                await _context.SaveChangesAsync();
            }
        }
    }
}