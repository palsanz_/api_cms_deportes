﻿using System.Linq;
using System.Threading.Tasks;
using EFCoreDatabase.Context;
using EFCoreDatabase.Models;
using EFCoreDatabase.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace EFCoreDatabase.Repositories
{
    public class DeporteRepository : IDeporteRepository
    {
        private readonly EurovalDbContext _context;

        public DeporteRepository(EurovalDbContext context)
        {
            _context = context;
        }

        public IQueryable<Deporte> Get()
        {
            return _context.Deporte.AsNoTracking();
        }

        public IQueryable<Deporte> GetById(int id)
        {
            return _context.Deporte.Where(m => m.Id == id);
        }

        public async Task<Deporte> Add(Deporte deporte)
        {
            if (deporte != null)
            {
                await _context.AddAsync(deporte);
                await _context.SaveChangesAsync();
            }

            return deporte;
        }

        public async Task Update(Deporte deporte)
        {
            if (deporte != null)
            {
                _context.Deporte.Update(deporte);
                await _context.SaveChangesAsync();
            }
        }

        public async Task Delete(Deporte deporte)
        {
            if (deporte != null)
            {
                _context.Deporte.Remove(deporte);
                await _context.SaveChangesAsync();
            }
        }
    }
}