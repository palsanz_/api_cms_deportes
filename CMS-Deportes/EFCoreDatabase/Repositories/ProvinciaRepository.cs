﻿using System.Linq;
using EFCoreDatabase.Context;
using EFCoreDatabase.Models;
using EFCoreDatabase.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace EFCoreDatabase.Repositories
{
    public class ProvinciaRepository : IProvinciaRepository
    {
        private readonly EurovalDbContext _context;

        public ProvinciaRepository(EurovalDbContext context)
        {
            _context = context;
        }

        public IQueryable<Provincia> Get()
        {
            return _context.Provincia.AsNoTracking();
        }

        public IQueryable<Provincia> GetById(int id)
        {
            return _context.Provincia.Where(m => m.Id == id);
        }
    }
}