﻿using System;
using System.Linq;
using System.Threading.Tasks;
using EFCoreDatabase.Models;

namespace EFCoreDatabase.Repositories.Interfaces
{
    public interface IReservaRepository
    {
        /// <summary>
        ///     Obtenemos todas las reservas.
        /// </summary>
        /// <returns></returns>
        IQueryable<Reserva> Get();

        /// <summary>
        ///     Obtenemos las reservas por id de reserva
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        IQueryable<Reserva> GetById(long id);

        /// <summary>
        ///     Obtenemos las reservas de la fecha pasada como parámetro
        /// </summary>
        /// <param name="fecha"></param>
        /// <returns></returns>
        IQueryable<Reserva> GetByDate(DateTime fecha);

        /// <summary>
        ///     Agrega una nueva reserva
        /// </summary>
        /// <param name="reserva"></param>
        /// <returns></returns>
        Task<Reserva> Add(Reserva reserva);

        /// <summary>
        ///     Actualiza una reserva
        /// </summary>
        /// <param name="reserva"></param>
        /// <returns></returns>
        Task Update(Reserva reserva);

        /// <summary>
        ///     Elimina una reserva
        /// </summary>
        /// <param name="reserva"></param>
        /// <returns></returns>
        Task Delete(Reserva reserva);
    }
}