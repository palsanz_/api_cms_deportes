﻿using System.Linq;
using EFCoreDatabase.Models;

namespace EFCoreDatabase.Repositories.Interfaces
{
    public interface IPoblacionRepository
    {
        IQueryable<Poblacion> Get();
        IQueryable<Poblacion> GetByProvincia(int provincia);
        IQueryable<Poblacion> GetById(int id);
    }
}