﻿using System.Linq;
using System.Threading.Tasks;
using EFCoreDatabase.Models;

namespace EFCoreDatabase.Repositories.Interfaces
{
    public interface ISocioRepository
    {
        /// <summary>
        /// Obtenemos todos los socios
        /// </summary>
        /// <returns></returns>
        IQueryable<Socio> Get();

        /// <summary>
        ///     Obtenemos el socio por id
        /// </summary>
        /// <param name="id">Id del socio</param>
        /// <returns></returns>
        IQueryable<Socio> GetById(long id);

        /// <summary>
        ///     Obtenemos los socios con coincidencia en el nombre
        /// </summary>
        /// <returns></returns>
        IQueryable<Socio> GetByName(string name);

        /// <summary>
        ///     Agrega un nuevo SocioId
        /// </summary>
        /// <param name="socio"></param>
        /// <returns></returns>
        Task<Socio> Add(Socio socio);

        /// <summary>
        ///     Actualiza un SocioId
        /// </summary>
        /// <param name="socio"></param>
        /// <returns></returns>
        Task Update(Socio socio);

        /// <summary>
        ///     Elimina un SocioId
        /// </summary>
        /// <param name="socio"></param>
        /// <returns></returns>
        Task Delete(Socio socio);
    }
}