﻿using System.Linq;
using System.Threading.Tasks;
using EFCoreDatabase.Models;


namespace EFCoreDatabase.Repositories.Interfaces
{
    /// <summary>
    ///     Interfaz IDeporteRepository
    /// </summary>
    public interface IDeporteRepository
    {
        /// <summary>
        ///     Obtenemos el deporte por Id
        /// </summary>
        /// <param name="id">Identificador del deporte</param>
        /// <returns></returns>
        IQueryable<Deporte> GetById(int id);

        /// <summary>
        ///     Obtenemos todos los deportes.
        /// </summary>
        /// <returns></returns>
        IQueryable<Deporte> Get();

        /// <summary>
        ///     Agrega un nuevo deporte
        /// </summary>
        /// <param name="deporte"></param>
        /// <returns></returns>
        Task<Deporte> Add(Deporte deporte);

        /// <summary>
        ///     Actualiza un deporte
        /// </summary>
        /// <param name="deporte"></param>
        /// <returns></returns>
        Task Update(Deporte deporte);

        /// <summary>
        ///     Elimina un deporte
        /// </summary>
        /// <param name="deporte"></param>
        /// <returns></returns>
        Task Delete(Deporte deporte);
    }
}