﻿using System;
using System.Linq;
using System.Threading.Tasks;
using EFCoreDatabase.Models;

namespace EFCoreDatabase.Repositories.Interfaces
{
    /// <summary>
    ///     Interfaz IPistaRepository
    /// </summary>
    public interface IPistaRepository
    {
        /// <summary>
        /// Obtenemos un listado de pistas
        /// </summary>
        /// <returns></returns>
        IQueryable<Pista> Get();

        /// <summary>
        ///     Obtenemos la pista por Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        IQueryable<Pista> GetById(int id);

        /// <summary>
        ///     Obtenemos las pistas disponibles para una fecha dada
        /// </summary>
        /// <param name="fecha">Fecha e la consulta</param>
        /// <param name="deporte">DeporteId al que pertenece la pista</param>
        /// <param name="socio">SocioId que quiere realizar la reserva</param>
        /// <returns></returns>
        IQueryable<Pista> GetDisponiblesByFilter(DateTime fecha, int deporte, long? socio);

        /// <summary>
        ///     Agrega una nueva pista
        /// </summary>
        /// <param name="pista"></param>
        /// <returns></returns>
        Task<Pista> Add(Pista pista);

        /// <summary>
        ///     Actualiza una pista
        /// </summary>
        /// <param name="pista"></param>
        /// <returns></returns>
        Task Update(Pista pista);

        /// <summary>
        ///     Elimina una pista
        /// </summary>
        /// <param name="pista"></param>
        /// <returns></returns>
        Task Delete(Pista pista);
    }
}