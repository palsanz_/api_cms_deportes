﻿using System.Linq;
using EFCoreDatabase.Models;

namespace EFCoreDatabase.Repositories.Interfaces
{
    public interface IProvinciaRepository
    {
        IQueryable<Provincia> Get();
        IQueryable<Provincia> GetById(int id);
    }
}