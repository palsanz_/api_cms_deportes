﻿using System.Linq;
using System.Threading.Tasks;
using EFCoreDatabase.Models;

namespace EFCoreDatabase.Repositories.Interfaces
{
    public interface IUsuarioRepository
    {
        /// <summary>
        /// Obtenemos todos los usuarios
        /// </summary>
        /// <returns></returns>
        IQueryable<Usuario> Get();

        /// <summary>
        ///     Obtenemos el usuario por id
        /// </summary>
        /// <param name="id">Id del UsuarioId</param>
        /// <returns></returns>
        IQueryable<Usuario> GetById(int id);

        /// <summary>
        ///     Obtenemos el usuario por su identificación
        /// </summary>
        /// <param name="userName">nombre de usuario</param>
        /// <returns></returns>
        IQueryable<Usuario> GetByUserName(string userName);

        /// <summary>
        ///     Obtenemos los Usuarios con coincidencia en el nombre
        /// </summary>
        /// <returns></returns>
        IQueryable<Usuario> GetByName(string name);

        /// <summary>
        ///     Agrega un nuevo UsuarioId
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        Task<Usuario> Add(Usuario usuario);

        /// <summary>
        ///     Actualiza un UsuarioId
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        Task Update(Usuario usuario);

        /// <summary>
        ///     Elimina un UsuarioId
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        Task Delete(Usuario usuario);
    }
}