﻿using System;
using System.Linq;
using System.Threading.Tasks;
using EFCoreDatabase.Context;
using EFCoreDatabase.Models;
using EFCoreDatabase.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace EFCoreDatabase.Repositories
{
    public class PistaRepository : IPistaRepository
    {
        private readonly EurovalDbContext _context;

        public PistaRepository(EurovalDbContext context)
        {
            _context = context;
        }

        public IQueryable<Pista> Get()
        {
            return _context.Pista.AsNoTracking();
        }

        public IQueryable<Pista> GetById(int id)
        {
            return _context.Pista.Where(m => m.Id == id);
        }

        public IQueryable<Pista> GetDisponiblesByFilter(DateTime fecha, int deporte, long? socio)
        {
            //IQueryable<Pista> disponibles = null;
            //IQueryable<Pista> disponibles = _context
            //    .Pista
            //    .FromSql(@"SELECT * FROM PISTA P WHERE  deporte = {0}  
            //               AND NOT EXISTS (
            //                    SELECT R.[pista]      
            //           FROM Reserva  R		
            //           JOIN Deporte D ON D.id = P.deporte
            //           WHERE P.id = R.pista and
            //        fecha='{1:yyyyMMdd HH:mm:ss}' AND D.id = {0})", deporte, fecha)
            //    .AsNoTracking();

            IQueryable<Pista> disponibles = (from pista in _context.Pista
                                             from reserva in pista.Reserva
                                             where pista.DeporteId == deporte &&
                                                   reserva.Fecha.Date == fecha.Date
                                             select pista);



            return disponibles;
        }

        public async Task<Pista> Add(Pista pista)
        {
            if (pista != null)
            {
                await _context.AddAsync(pista);
                await _context.SaveChangesAsync();
            }

            return pista;
        }

        public async Task Update(Pista pista)
        {
            if (pista != null)
            {
                _context.Pista.Update(pista);
                await _context.SaveChangesAsync();
            }
        }

        public async Task Delete(Pista pista)
        {
            if (pista != null)
            {
                _context.Pista.Remove(pista);
                await _context.SaveChangesAsync();
            }
        }
    }
}