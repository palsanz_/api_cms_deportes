﻿using System.Linq;
using System.Threading.Tasks;
using EFCoreDatabase.Context;
using EFCoreDatabase.Models;
using EFCoreDatabase.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace EFCoreDatabase.Repositories
{
    public class UsuarioRepository : IUsuarioRepository
    {
        private readonly EurovalDbContext _context;

        public UsuarioRepository(EurovalDbContext context)
        {
            _context = context;
        }

        public IQueryable<Usuario> Get()
        {
            return _context.Usuario.AsNoTracking();
        }

        public IQueryable<Usuario> GetById(int id)
        {
            return _context.Usuario.Where(m => m.Id == id);
        }

        public IQueryable<Usuario> GetByUserName(string userName)
        {
            var nameQuery = _context
                .Usuario
                .Where(query => EF.Functions.Like(query.Usuario1, $"%{userName}%"))
                .AsNoTracking();

            return nameQuery;
        }

        public IQueryable<Usuario> GetByName(string name)
        {
            var nameQuery = _context
                .Usuario
                .Where(query => EF.Functions.Like(query.Nombre, $"%{name}%"))
                .AsNoTracking();

            return nameQuery;
        }

        public async Task<Usuario> Add(Usuario usuario)
        {
            await _context.AddAsync(usuario);
            await _context.SaveChangesAsync();

            return usuario;
        }

        public async Task Update(Usuario usuario)
        {
            if (usuario != null)
            {
                _context.Usuario.Update(usuario);
                await _context.SaveChangesAsync();
            }
        }

        public async Task Delete(Usuario usuario)
        {
            if (usuario != null)
            {
                //Borrado lógico
                usuario.Borrado = true;
                _context.Usuario.Update(usuario);
                await _context.SaveChangesAsync();
            }
        }
    }
}