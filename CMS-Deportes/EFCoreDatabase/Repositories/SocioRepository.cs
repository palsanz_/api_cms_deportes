﻿using System.Linq;
using System.Threading.Tasks;
using EFCoreDatabase.Context;
using EFCoreDatabase.Models;
using EFCoreDatabase.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace EFCoreDatabase.Repositories
{
    public class SocioRepository : ISocioRepository
    {
        private readonly EurovalDbContext _context;

        public SocioRepository(EurovalDbContext context)
        {
            _context = context;
        }

        public IQueryable<Socio> Get()
        {
            return _context.Socio.AsNoTracking();
        }

        public IQueryable<Socio> GetById(long id)
        {
            return _context.Socio.Where(m => m.Id == id);
        }

        public IQueryable<Socio> GetByName(string name)
        {
            var nameQuery = _context
                .Socio
                .Where(query => EF.Functions.Like(query.Nombre, $"%{name}%"))
                .AsNoTracking();

            return nameQuery;
        }

        public async Task<Socio> Add(Socio socio)
        {
            await _context.AddAsync(socio);
            await _context.SaveChangesAsync();

            return socio;
        }

        /// <summary>
        /// </summary>
        /// <param name="socio"></param>
        /// <returns></returns>
        public async Task Update(Socio socio)
        {
            if (socio != null)
            {
                _context.Socio.Update(socio);
                await _context.SaveChangesAsync();
            }
        }

        /// <summary>
        ///     Borrado del SocioId.
        /// </summary>
        /// <param name="socio"></param>
        /// <returns></returns>
        public async Task Delete(Socio socio)
        {
            ////Para los socios realizamos un borrado lógico para que sigan estando en la bd para el histórico de registros.
            if (socio != null)
            {
                socio.Borrado = true;
                _context.Socio.Update(socio);
                await _context.SaveChangesAsync();
            }
        }
    }
}