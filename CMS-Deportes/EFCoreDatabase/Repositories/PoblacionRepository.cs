﻿using System.Linq;
using EFCoreDatabase.Context;
using EFCoreDatabase.Models;
using EFCoreDatabase.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace EFCoreDatabase.Repositories
{
    public class PoblacionRepository : IPoblacionRepository
    {
        private readonly EurovalDbContext _context;

        public PoblacionRepository(EurovalDbContext context)
        {
            _context = context;
        }

        public IQueryable<Poblacion> Get()
        {
            return _context.Poblacion.AsNoTracking();
        }

        public IQueryable<Poblacion> GetByProvincia(int provincia)
        {
            return _context.Poblacion.Where(m => m.ProvinciaId == provincia).AsNoTracking();
        }

        public IQueryable<Poblacion> GetById(int id)
        {
            return _context.Poblacion.Where(m => m.Id == id);
        }
    }
}