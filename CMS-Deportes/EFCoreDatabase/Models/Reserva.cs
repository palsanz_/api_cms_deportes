﻿using System;

namespace EFCoreDatabase.Models
{
    public class Reserva
    {
        public long Id { get; set; }
        public DateTime Fecha { get; set; }
        public long SocioId { get; set; }
        public int PistaId { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaActualizacion { get; set; }
        public int UsuarioId { get; set; }
        public virtual Pista Pista { get; set; }
        public virtual Socio Socio { get; set; }
        public virtual Usuario Usuario { get; set; }
    }
}