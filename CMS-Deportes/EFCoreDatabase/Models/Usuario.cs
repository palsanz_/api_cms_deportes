﻿using System.Collections.Generic;

namespace EFCoreDatabase.Models
{
    public class Usuario
    {
        public Usuario()
        {
            Reserva = new HashSet<Reserva>();
            Socio = new HashSet<Socio>();
        }

        public int Id { get; set; }
        public string Usuario1 { get; set; }
        public string Nombre { get; set; }
        public string Apellido1 { get; set; }
        public string Apellido2 { get; set; }
        public string Email { get; set; }
        public bool? Activo { get; set; }
        public bool Borrado { get; set; }
        public virtual ICollection<Reserva> Reserva { get; set; }
        public virtual ICollection<Socio> Socio { get; set; }
    }
}