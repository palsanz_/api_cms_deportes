﻿using System;
using System.Collections.Generic;

namespace EFCoreDatabase.Models
{
    public class Socio
    {
        public Socio()
        {
            Reserva = new HashSet<Reserva>();
        }

        public long Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido1 { get; set; }
        public string Apellido2 { get; set; }
        public string Email { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public string Direccion { get; set; }
        public int? PoblacionId { get; set; }
        public string CodigoPostal { get; set; }
        public DateTime FechaAlta { get; set; }
        public DateTime? FechaActualizacion { get; set; }
        public int UsuarioId { get; set; }
        public bool? Borrado { get; set; }
        public virtual Poblacion Poblacion { get; set; }
        public virtual Usuario Usuario { get; set; }
        public virtual ICollection<Reserva> Reserva { get; set; }
    }
}