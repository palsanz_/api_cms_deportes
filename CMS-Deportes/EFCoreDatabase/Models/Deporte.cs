﻿using System.Collections.Generic;

namespace EFCoreDatabase.Models
{
    public class Deporte
    {
        public Deporte()
        {
            Pista = new HashSet<Pista>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public virtual ICollection<Pista> Pista { get; set; }
    }
}