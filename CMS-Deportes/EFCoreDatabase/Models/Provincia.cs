﻿using System.Collections.Generic;

namespace EFCoreDatabase.Models
{
    public class Provincia
    {
        public Provincia()
        {
            Poblacion = new HashSet<Poblacion>();
        }

        public int Id { get; set; }
        public string Cod { get; set; }
        public string Nombre { get; set; }
        public virtual ICollection<Poblacion> Poblacion { get; set; }
        
    }
}