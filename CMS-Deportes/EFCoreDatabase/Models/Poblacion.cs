﻿using System.Collections.Generic;

namespace EFCoreDatabase.Models
{
    public class Poblacion
    {
        public Poblacion()
        {
            Socio = new HashSet<Socio>();
        }

        public int Id { get; set; }
        public int ProvinciaId { get; set; }
        public string Nombre { get; set; }
        public virtual Provincia Provincia { get; set; }
        public virtual ICollection<Socio> Socio { get; set; }
    }
}