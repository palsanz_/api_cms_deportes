﻿using System.Collections.Generic;

namespace EFCoreDatabase.Models
{
    public class Pista
    {
        public Pista()
        {
            Reserva = new HashSet<Reserva>();
        }

        public int Id { get; set; }
        public int DeporteId { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public virtual Deporte Deporte { get; set; }
        public virtual ICollection<Reserva> Reserva { get; set; }
    }
}