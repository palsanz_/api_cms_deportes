﻿using EFCoreDatabase.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EFCoreDatabase.Configurations
{
    public class SocioConfiguration : IEntityTypeConfiguration<Socio>
    {
        public void Configure(EntityTypeBuilder<Socio> builder)
        {
            builder.Property(e => e.Id).HasColumnName("id");

            builder.Property(e => e.Apellido1)
                .IsRequired()
                .HasColumnName("apellido1")
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(e => e.Apellido2)
                .IsRequired()
                .HasColumnName("apellido2")
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(e => e.Borrado)
                .IsRequired()
                .HasColumnName("borrado")
                .HasDefaultValueSql("((1))");

            builder.Property(e => e.CodigoPostal)
                .HasColumnName("codigoPostal")
                .HasMaxLength(10)
                .IsUnicode(false);

            builder.Property(e => e.Direccion)
                .HasColumnName("direccion")
                .HasMaxLength(500)
                .IsUnicode(false);

            builder.Property(e => e.Email)
                .IsRequired()
                .HasColumnName("email")
                .HasMaxLength(100)
                .IsUnicode(false);

            builder.Property(e => e.FechaActualizacion)
                .HasColumnName("fechaActualizacion")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            builder.Property(e => e.FechaAlta)
                .HasColumnName("fechaAlta")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            builder.Property(e => e.FechaNacimiento)
                .HasColumnName("fechaNacimiento")
                .HasColumnType("datetime");

            builder.Property(e => e.Nombre)
                .IsRequired()
                .HasColumnName("nombre")
                .HasMaxLength(100)
                .IsUnicode(false);

            builder.Property(e => e.PoblacionId).HasColumnName("poblacion");
            builder.Property(e => e.UsuarioId).HasColumnName("usuario");

            builder.HasOne(d => d.Poblacion)
                .WithMany(p => p.Socio)
                .HasForeignKey(d => d.PoblacionId)
                .HasConstraintName("FK_Socio_Pueblo");

            builder.HasOne(d => d.Usuario)
                .WithMany(p => p.Socio)
                .HasForeignKey(d => d.UsuarioId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Socio_Usuario");
        }
    }
}