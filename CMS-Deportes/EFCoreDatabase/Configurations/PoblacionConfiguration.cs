﻿using EFCoreDatabase.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EFCoreDatabase.Configurations
{
    internal class PoblacionConfiguration : IEntityTypeConfiguration<Poblacion>
    {
        public void Configure(EntityTypeBuilder<Poblacion> builder)
        {
            builder.Property(e => e.Id).HasColumnName("id");

            builder.Property(e => e.Nombre)
                .IsRequired()
                .HasColumnName("nombre")
                .HasMaxLength(200)
                .IsUnicode(false);

            builder.Property(e => e.ProvinciaId).HasColumnName("provincia");

            builder.HasOne(d => d.Provincia)
                .WithMany(p => p.Poblacion)
                .HasForeignKey(d => d.ProvinciaId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Pueblo_Provincia");
        }
    }
}