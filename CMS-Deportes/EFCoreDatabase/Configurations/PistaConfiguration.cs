﻿using EFCoreDatabase.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EFCoreDatabase.Configurations
{
    public class PistaConfiguration : IEntityTypeConfiguration<Pista>
    {
        public void Configure(EntityTypeBuilder<Pista> builder)
        {
            builder.Property(e => e.Id)
                .HasColumnName("id");

            builder.Property(e => e.DeporteId).HasColumnName("deporte");

            builder.Property(e => e.Descripcion)
                .HasColumnName("descripcion")
                .HasMaxLength(200)
                .IsUnicode(false);

            builder.Property(e => e.Nombre)
                .IsRequired()
                .HasColumnName("nombre")
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.HasOne(d => d.Deporte)
                .WithMany(p => p.Pista)
                .HasForeignKey(d => d.DeporteId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Pista_Deporte");
        }
    }
}