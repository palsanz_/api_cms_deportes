﻿using EFCoreDatabase.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EFCoreDatabase.Configurations
{
    public class UsuarioConfiguration : IEntityTypeConfiguration<Usuario>
    {
        public void Configure(EntityTypeBuilder<Usuario> builder)
        {
            builder.Property(e => e.Id).HasColumnName("id");

            builder.Property(e => e.Activo)
                .IsRequired()
                .HasColumnName("activo")
                .HasDefaultValueSql("((1))");

            builder.Property(e => e.Apellido1)
                .IsRequired()
                .HasColumnName("apellido1")
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(e => e.Apellido2)
                .IsRequired()
                .HasColumnName("apellido2")
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(e => e.Borrado).HasColumnName("borrado");

            builder.Property(e => e.Email)
                .IsRequired()
                .HasColumnName("email")
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(e => e.Nombre)
                .IsRequired()
                .HasColumnName("nombre")
                .HasMaxLength(100)
                .IsUnicode(false);

            builder.Property(e => e.Usuario1)
                .IsRequired()
                .HasColumnName("usuario")
                .HasMaxLength(20)
                .IsUnicode(false);
        }
    }
}