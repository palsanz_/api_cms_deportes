﻿using EFCoreDatabase.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EFCoreDatabase.Configurations
{
    internal class DeporteConfiguration : IEntityTypeConfiguration<Deporte>
    {
        public void Configure(EntityTypeBuilder<Deporte> builder)
        {
            builder.Property(e => e.Id)
                .HasColumnName("id");

            builder.Property(e => e.Nombre)
                .IsRequired()
                .HasColumnName("nombre")
                .HasMaxLength(100)
                .IsUnicode(false);
        }
    }
}