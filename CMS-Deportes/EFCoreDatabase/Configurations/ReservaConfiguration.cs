﻿using EFCoreDatabase.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EFCoreDatabase.Configurations
{
    internal class ReservaConfiguration : IEntityTypeConfiguration<Reserva>
    {
        public void Configure(EntityTypeBuilder<Reserva> builder)
        {
            builder.Property(e => e.Id).HasColumnName("id");

            builder.Property(e => e.Fecha)
                .HasColumnName("fecha")
                .HasColumnType("datetime");

            builder.Property(e => e.FechaActualizacion)
                .HasColumnName("fechaActualizacion")
                .HasColumnType("datetime");

            builder.Property(e => e.FechaCreacion)
                .HasColumnName("fechaCreacion")
                .HasColumnType("datetime");

            builder.Property(e => e.PistaId).HasColumnName("pista");

            builder.Property(e => e.SocioId).HasColumnName("socio");

            builder.Property(e => e.UsuarioId).HasColumnName("usuario");

            builder.HasOne(d => d.Pista)
                .WithMany(p => p.Reserva)
                .HasForeignKey(d => d.PistaId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Reserva_Pista");

            builder.HasOne(d => d.Socio)
                .WithMany(p => p.Reserva)
                .HasForeignKey(d => d.SocioId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Reserva_Socio");

            builder.HasOne(d => d.Usuario)
                .WithMany(p => p.Reserva)
                .HasForeignKey(d => d.UsuarioId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Reserva_Usuario");
        }
    }
}