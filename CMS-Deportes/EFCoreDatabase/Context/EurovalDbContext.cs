﻿using EFCoreDatabase.Configurations;
using EFCoreDatabase.Models;
using Microsoft.EntityFrameworkCore;

namespace EFCoreDatabase.Context
{
    public class EurovalDbContext : DbContext
    {
        public virtual DbSet<Deporte> Deporte { get; set; }
        public virtual DbSet<Pista> Pista { get; set; }
        public virtual DbSet<Poblacion> Poblacion { get; set; }
        public virtual DbSet<Provincia> Provincia { get; set; }
        public virtual DbSet<Reserva> Reserva { get; set; }
        public virtual DbSet<Socio> Socio { get; set; }
        public virtual DbSet<Usuario> Usuario { get; set; }
        
        public EurovalDbContext(DbContextOptions<EurovalDbContext> options): base(options){}

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
                optionsBuilder.UseSqlServer("Data Source=192.168.1.4;Initial Catalog=EUROVAL_TEST;Persist Security Info=True;User ID=sa;Password=spp27091983;Encrypt=False;TrustServerCertificate=True");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new DeporteConfiguration());
            modelBuilder.ApplyConfiguration(new PistaConfiguration());
            modelBuilder.ApplyConfiguration(new PoblacionConfiguration());
            modelBuilder.ApplyConfiguration(new ProvinciaConfiguration());
            modelBuilder.ApplyConfiguration(new ReservaConfiguration());
            modelBuilder.ApplyConfiguration(new SocioConfiguration());
            modelBuilder.ApplyConfiguration(new UsuarioConfiguration());
        }
    }
}