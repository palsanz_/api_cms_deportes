﻿CREATE TABLE [dbo].[Poblacion] (
    [id]        INT           IDENTITY (1, 1) NOT NULL,
    [provincia] INT           NOT NULL,
    [nombre]    VARCHAR (200) NOT NULL,
    CONSTRAINT [PK_Pueblo] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [FK_Pueblo_Provincia] FOREIGN KEY ([provincia]) REFERENCES [dbo].[Provincia] ([id])
);

