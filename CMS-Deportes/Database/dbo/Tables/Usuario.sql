﻿CREATE TABLE [dbo].[Usuario] (
    [id]        INT           IDENTITY (1, 1) NOT NULL,
    [usuario]   VARCHAR (20)  NOT NULL,
    [nombre]    VARCHAR (100) NOT NULL,
    [apellido1] VARCHAR (50)  NOT NULL,
    [apellido2] VARCHAR (50)  NOT NULL,
    [email]     VARCHAR (50)  NOT NULL,
    [activo]    BIT           CONSTRAINT [DF_Usuario_activo] DEFAULT ((1)) NOT NULL,
    [borrado]   BIT           CONSTRAINT [DF_Usuario_borrado] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED ([id] ASC)
);



