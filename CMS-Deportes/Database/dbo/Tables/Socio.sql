﻿CREATE TABLE [dbo].[Socio] (
    [id]                 BIGINT        IDENTITY (1, 1) NOT NULL,
    [nombre]             VARCHAR (100) NOT NULL,
    [apellido1]          VARCHAR (50)  NOT NULL,
    [apellido2]          VARCHAR (50)  NOT NULL,
    [email]              VARCHAR (100) NOT NULL,
    [fechaNacimiento]    DATETIME      NULL,
    [direccion]          VARCHAR (500) NULL,
    [poblacion]          INT           NULL,
    [codigoPostal]       VARCHAR (10)  NULL,
    [fechaAlta]          DATETIME      CONSTRAINT [DF_Socio_fechaAlta] DEFAULT (getdate()) NOT NULL,
    [fechaActualizacion] DATETIME      CONSTRAINT [DF_Socio_fechaActualizacion] DEFAULT (getdate()) NULL,
    [usuario]            INT           NOT NULL,
    [borrado]            BIT           CONSTRAINT [DF_Socio_activo] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_Socio] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [FK_Socio_Pueblo] FOREIGN KEY ([poblacion]) REFERENCES [dbo].[Poblacion] ([id]),
    CONSTRAINT [FK_Socio_Usuario] FOREIGN KEY ([usuario]) REFERENCES [dbo].[Usuario] ([id])
);







