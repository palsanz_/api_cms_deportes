﻿CREATE TABLE [dbo].[Pista] (
    [id]          INT           IDENTITY (1, 1) NOT NULL,
    [deporte]     INT           NOT NULL,
    [nombre]      VARCHAR (50)  NOT NULL,
    [descripcion] VARCHAR (200) NULL,
    CONSTRAINT [PK_Pista_1] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [FK_Pista_Deporte] FOREIGN KEY ([deporte]) REFERENCES [dbo].[Deporte] ([id])
);



