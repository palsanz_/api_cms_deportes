﻿CREATE TABLE [dbo].[Reserva] (
    [id]                 BIGINT   IDENTITY (1, 1) NOT NULL,
    [fecha]              DATETIME NOT NULL,
    [socio]              BIGINT   NOT NULL,
    [pista]              INT      NOT NULL,
    [fechaCreacion]      DATETIME NOT NULL,
    [fechaActualizacion] DATETIME NOT NULL,
    [usuario]            INT      NOT NULL,
    CONSTRAINT [PK_Reserva] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [FK_Reserva_Pista] FOREIGN KEY ([pista]) REFERENCES [dbo].[Pista] ([id]),
    CONSTRAINT [FK_Reserva_Socio] FOREIGN KEY ([socio]) REFERENCES [dbo].[Socio] ([id]),
    CONSTRAINT [FK_Reserva_Usuario] FOREIGN KEY ([usuario]) REFERENCES [dbo].[Usuario] ([id])
);



