﻿CREATE TABLE [dbo].[Provincia] (
    [id]     INT           NOT NULL,
    [cod]    VARCHAR (10)  NOT NULL,
    [nombre] VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_Provincia] PRIMARY KEY CLUSTERED ([id] ASC)
);

