﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Service.Services.Interfaces;
using Service.ViewModels;

namespace API.Controllers
{
    /// <summary>
    /// Controlador encargado del CRUD de deportes.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class DeportesController : ControllerBase
    {
        private readonly IDeporteService _deporteService;

        /// <summary>
        /// Constructor por defecto.
        /// </summary>
        /// <param name="deporteService"></param>
        public DeportesController(IDeporteService deporteService)
        {
            _deporteService = deporteService ?? throw new ArgumentNullException(nameof(deporteService));
        }

        /// <summary>
        /// Obtener deportes disponibles.
        /// </summary>
        /// <returns></returns>
        // GET: api/Deportes
        [HttpGet]
        public async Task<ActionResult<List<VMDeporte>>> GetDeporte()
        {
            return await _deporteService.Get();
        }


        /// <summary>
        /// Obtenemos el deporte con el identificador pasado por parámetro.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Deportes/5
        [HttpGet("{id:int}")]
        public async Task<ActionResult<VMDeporte>> GetDeporte(int id)
        {
            var deporte = await _deporteService.GetById(id);
            if (deporte == null) return NotFound();
            return deporte;
        }


        /// <summary>
        /// Actualiza un deporte con un Id Dado.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="deporte"></param>
        /// <returns></returns>
        // PUT: api/Deportes/5
        [HttpPut("{id:int}")]
        public async Task<IActionResult> PutDeporte(int id, VMDeporte deporte)
        {

            if (id != deporte.Id)
                return BadRequest();
            try
            {
                var deporteUpdated = await _deporteService.Update(deporte);
                return Ok(deporteUpdated);
            }
            catch (Exception)
            {
                if (!DeporteExists(id))
                    return NotFound();
                throw;
            }
        }


        /// <summary>
        /// Inserta un deporte
        /// </summary>
        /// <param name="deporte"></param>
        /// <returns></returns>
        // POST: api/Deportes
        [HttpPost]
        public async Task<ActionResult<VMDeporte>> PostDeporte(VMDeporte deporte)
        {
            try
            {
                var deporteOk = await _deporteService.Add(deporte);
                return Ok(deporteOk);
            }
            catch (DbUpdateException)
            {
                if (DeporteExists(deporte.Id))
                    return Conflict();
                throw;
            }
        }

        // DELETE: api/Deportes/5
        /// <summary>
        /// Borra un deporte.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        public async Task<ActionResult<bool>> DeleteDeporte(int id)
        {
            try
            {
                if (await _deporteService.Delete(id))
                    return Ok();
            }
            catch (Exception)
            {
                if (!DeporteExists(id))
                    return NotFound();
                throw;
            }

            return BadRequest("Se ha producido un error");
        }

        private bool DeporteExists(int id)
        {
            return _deporteService.GetById(id) != null;
        }
    }
}