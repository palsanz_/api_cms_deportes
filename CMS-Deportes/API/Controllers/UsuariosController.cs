﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EFCoreDatabase.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Service.Services.Interfaces;
using Service.ViewModels;

namespace API.Controllers
{
    /// <summary>
    /// Controlador de USuarios
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class UsuariosController : ControllerBase
    {
        private readonly IUsuarioService _usuarioService;

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        /// <param name="usuarioService"></param>
        public UsuariosController(IUsuarioService usuarioService)
        {
            _usuarioService = usuarioService ?? throw new ArgumentNullException(nameof(usuarioService));
        }

        // GET: api/Usuarios
        /// <summary>
        /// Obtenemos los usuarios del sistema
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<VMUsuario>> GetUsuario()
        {
            return await _usuarioService.Get();
        }

        // GET: api/Usuarios/5
        /// <summary>
        /// Obtenemos la información del usuario indicado
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        public async Task<ActionResult<VMUsuario>> GetUsuario(int id)
        {
            var usuario = await _usuarioService.GetById(id);
            if (usuario == null) return NotFound();
            return usuario;
        }

        // GET: api/Usuarios/5
        /// <summary>
        /// Obtenemos los usuarios filtrados por nommbre
        /// </summary>
        /// <param name="nombre"></param>
        /// <returns></returns>
        [HttpGet("{nombre:alpha}")]
        public async Task<List<VMUsuario>> GetUsuarios(string nombre)
        {
            return await _usuarioService.GetByName(nombre);
        }

        // PUT: api/Usuarios/5
        /// <summary>
        /// Actualizamos el usuario indicado
        /// </summary>
        /// <param name="id"></param>
        /// <param name="usuario"></param>
        /// <returns></returns>
        [HttpPut("{id:int}")]
        public async Task<IActionResult> PutUsuario(int id, VMUsuario usuario)
        {
            if (id != usuario.Id)
                return BadRequest();

            try
            {
                var usuarioUpdated = await _usuarioService.Update(usuario);
                return Ok(usuarioUpdated);
            }
            catch (Exception)
            {
                if (!UsuarioExists(id))
                    return NotFound();
                throw;
            }
        }

        // POST: api/Usuarios
        /// <summary>
        /// Insertamos un nuevo usuario
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Usuario>> PostUsuario(VMUsuario usuario)
        {
            try
            {
                var usuarioOk = await _usuarioService.Add(usuario);
                return Ok(usuarioOk);
            }
            catch (DbUpdateException)
            {
                if (UsuarioExists(usuario.Id))
                    return Conflict();
                throw;
            }
        }

        // DELETE: api/Usuarios/5
        /// <summary>
        /// Borramos un usuario
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        public async Task<ActionResult<Usuario>> DeleteUsuario(int id)
        {
            try
            {
                if (await _usuarioService.Delete(id))
                    return Ok();
            }
            catch (Exception)
            {
                if (!UsuarioExists(id))
                    return NotFound();
                throw;
            }

            return BadRequest("Se ha producido un error");
        }

        private bool UsuarioExists(int id)
        {
            return _usuarioService.GetById(id) != null;
        }
    }
}