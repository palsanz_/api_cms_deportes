﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Service.Services;
using Service.ViewModels;

namespace API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class PoblacionesController : ControllerBase
    {
        private readonly IPoblacionService _poblacionService;

        /// <summary>
        /// Constructor por defecto.
        /// </summary>
        /// <param name="poblacionService"></param>
        public PoblacionesController(IPoblacionService poblacionService)
        {
            _poblacionService = poblacionService ?? throw new ArgumentNullException(nameof(poblacionService));
        }

        /// <summary>
        /// Obtenemos todas las poblaciones disponibles
        /// </summary>
        /// <returns></returns>
        // GET: api/Poblaciones
        [HttpGet]
        public async Task<ActionResult<List<VMPoblacion>>> GetPoblacion()
        {
            return await _poblacionService.Get();
        }

        /// <summary>
        /// Obtenemos todas las poblaciones disponibles
        /// </summary>
        /// <returns></returns>
        // GET: api/Poblaciones
        [HttpGet("Provincia/{provincia:int}")]
        public async Task<ActionResult<List<VMPoblacion>>> GetPoblacionByProvincia(int provincia)
        {
            return await _poblacionService.GetByProvincia(provincia);
        }

        /// <summary>
        /// Obtenemos la población con el identificador pasado por parámetro.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Poblaciones/5
        [HttpGet("{id:int}")]
        public async Task<ActionResult<VMPoblacion>> GetPoblacion(int id)
        {
            var poblacion = await _poblacionService.GetById(id);
            if (poblacion == null) return NotFound();
            return poblacion;
        }
    }
}
