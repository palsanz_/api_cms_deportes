﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EFCoreDatabase.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Service.Services.Interfaces;
using Service.ViewModels;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class PistasController : ControllerBase
    {

        private readonly IPistaService _pistaService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pistaService"></param>
        public PistasController(IPistaService pistaService)
        {
            _pistaService = pistaService;
        }

        // GET: api/Pistas
        /// <summary>
        /// Obtenemos todas las pistas creadas en el sistema
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<VMPistaDetalle>> GetPistas()
        {
            return await _pistaService.Get();
        }

        // GET: api/Pistas/Disponibles
        /// <summary>
        /// Obtenemos las pistas disponibles de un deporte en una fecha dada
        /// </summary>
        /// <param name="fecha"></param>
        /// <param name="deporteId"></param>
        /// <returns></returns>
        [HttpGet("Disponibles")]
        public async Task<List<VMPistaDetalle>> GetPistasDisponibles(DateTime fecha, int deporteId)
        {
            return await _pistaService.GetDisponiblesByFilter(fecha, deporteId, 0);
        }

        // GET: api/Pistas/5
        /// <summary>
        /// Obtenemos una pista para un id dado
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        public async Task<ActionResult<VMPistaDetalle>> GetPista(int id)
        {
            var pista = await _pistaService.GetById(id);
            if (pista == null) return NotFound();
            return pista;
        }

        // PUT: api/Pistas/5
        /// <summary>
        /// Actualizamos los datos de una pista
        /// </summary>
        /// <param name="id"></param>
        /// <param name="pista"></param>
        /// <returns></returns>
        [HttpPut("{id:int}")]
        public async Task<IActionResult> PutPista(int id, VMPista pista)
        {
            if (id != pista.Id)
                return BadRequest();

            try
            {
                var pistaUpdated = await _pistaService.Update(pista);
                return Ok(pistaUpdated);
            }
            catch (Exception)
            {
                if (!PistaExists(id))
                    return NotFound();
                throw;
            }
        }

        // POST: api/Pistas
        /// <summary>
        /// Creamos una nueva pista
        /// </summary>
        /// <param name="pista"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Pista>> PostPista(VMPista pista)
        {
            try
            {
                var pistaOk = await _pistaService.Add(pista);
                return Ok(pistaOk);
            }
            catch (DbUpdateException)
            {
                if (PistaExists(pista.Id))
                    return Conflict();
                throw;
            }
        }

        // DELETE: api/Pistas/5
        /// <summary>
        /// Borramos una pista
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        public async Task<ActionResult<Pista>> DeletePista(int id)
        {
            try
            {
                if (await _pistaService.Delete(id))
                    return Ok();
            }
            catch (Exception)
            {
                if (!PistaExists(id))
                    return NotFound();
                throw;
            }

            return BadRequest("Se ha producido un error");
        }

        private bool PistaExists(int id)
        {
            return _pistaService.GetById(id) != null;
        }
    }
}