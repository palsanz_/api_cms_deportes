﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Service.Services;
using Service.ViewModels;

namespace API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class ProvinciasController : ControllerBase
    {

        private readonly IProvinciaService _povinciaService;

        /// <summary>
        /// Constructor por defecto.
        /// </summary>
        /// <param name="provinciaService"></param>
        public ProvinciasController(IProvinciaService provinciaService)
        {
            _povinciaService = provinciaService ?? throw new ArgumentNullException(nameof(provinciaService));
        }

        /// <summary>
        /// Obtenemos todas las provincias disponibles
        /// </summary>
        /// <returns></returns>
        // GET: api/Provincia
        [HttpGet]
        public async Task<ActionResult<List<VMProvincia>>> GetProvincia()
        {
            return await _povinciaService.Get();
        }
       

        /// <summary>
        /// Obtenemos la provincia con el identificador pasado por parámetro.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Provincia/5
        [HttpGet("{id:int}")]
        public async Task<ActionResult<VMProvincia>> GetProvincia(int id)
        {
            var provincia = await _povinciaService.GetById(id);
            if (provincia == null) return NotFound();
            return provincia;
        }



    }
}
