﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EFCoreDatabase.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Service.Services.Interfaces;
using Service.ViewModels;

namespace API.Controllers
{
    /// <summary>
    /// Controlador de Reservas
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class ReservasController : ControllerBase
    {
        private readonly IReservaService _reservaService;

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        /// <param name="reservaService"></param>
        public ReservasController(IReservaService reservaService)
        {
            _reservaService = reservaService;
        }

        /// <summary>
        /// Obtiene listado de reservas a una fecha dada.
        /// </summary>
        /// <param name="fecha"></param>
        /// <returns></returns>
        // GET: api/Reservas/
        [HttpGet]
        public async Task<List<VMReservaDetalle>> GetReservas(DateTime fecha)
        {
            if(fecha.Year==1)
                return await _reservaService.Get();

            return await _reservaService.GetByDate(fecha);
        }

        /// <summary>
        /// Obtiene una reserva por Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Reservas/5
        [HttpGet("{id:long}")]
        public async Task<ActionResult<VMReservaDetalle>> GetReserva(long id)
        {

            var reserva = await _reservaService.GetById(id);
            if (reserva == null) return NotFound();
            return reserva;

        }

        /// <summary>
        /// Edita una reserva
        /// </summary>
        /// <param name="id"></param>
        /// <param name="reserva"></param>
        /// <returns></returns>
        // PUT: api/Reservas/5
        [HttpPut("{id:long}")]
        public async Task<IActionResult> PutReserva(long id, VMReserva reserva)
        {
            if (id != reserva.Id)
                return BadRequest();

            try
            {
                var pistaUpdated = await _reservaService.Update(reserva);
                return Ok(pistaUpdated);
            }
            catch (Exception)
            {
                if (!ReservaExists(id))
                    return NotFound();
                throw;
            }
        }

        /// <summary>
        /// Crea una reserva
        /// </summary>
        /// <param name="reserva"></param>
        /// <returns></returns>
        // POST: api/Reservas
        [HttpPost]
        public async Task<ActionResult<Reserva>> PostReserva(VMReserva reserva)
        {

            try
            {
                var deporteOk = await _reservaService.Add(reserva);
                return Ok(deporteOk);
            }
            catch (DbUpdateException)
            {
                if (ReservaExists(reserva.Id))
                    return Conflict();
                throw;
            }
        }

        // DELETE: api/Reservas/5
        /// <summary>
        /// Borra una reserva
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:long}")]
        public async Task<ActionResult<Reserva>> DeleteReserva(long id)
        {
            try
            {
                if (await _reservaService.Delete(id))
                    return Ok();
            }
            catch (Exception)
            {
                if (!ReservaExists(id))
                    return NotFound();
                throw;
            }

            return BadRequest("Se ha producido un error");
        }

        private bool ReservaExists(long id)
        {
            return _reservaService.GetById(id) != null;
        }
    }
}