﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EFCoreDatabase.Context;
using EFCoreDatabase.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Service.Services.Interfaces;
using Service.ViewModels;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class SociosController : ControllerBase
    {
        private readonly ISocioService _sociosService;

        /// <summary>
        /// Controlador de socios
        /// </summary>
        /// <param name="sociosService"></param>
        public SociosController(ISocioService sociosService)
        {
            _sociosService = sociosService;
        }


        // GET: api/Socios
        /// <summary>
        /// Obtenemos todos los socios dados de alta.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<VMSocioDetalle>> GetSocio()
        {
            return await _sociosService.Get();
        }

        // GET: api/Socios/5
        /// <summary>
        /// Obtenemos un socio para un Id dado
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        public async Task<ActionResult<VMSocioDetalle>> GetSocio(long id)
        {
            var socio = await _sociosService.GetById(id);
            if (socio == null) return NotFound();
            return socio;
        }

        // GET: api/Socios/5
        /// <summary>
        /// Obtendremos los socios que tengan coincidencia por nombre
        /// </summary>
        /// <param name="nombre"></param>
        /// <returns></returns>
        [HttpGet("{nombre:alpha}")]
        public async Task<List<VMSocioDetalle>> GetSocios(string nombre)
        {
            return await _sociosService.GetByName(nombre);
        }


        /// <summary>
        /// Actualizaremos el socio pasado como parámetro
        /// </summary>
        /// <param name="id"></param>
        /// <param name="socio"></param>
        /// <returns></returns>
        // PUT: api/Socios/5
        [HttpPut("{id:long}")]
        public async Task<IActionResult> PutSocio(long id, VMSocio socio)
        {
            if (id != socio.Id)
                return BadRequest();

            try
            {
                var socioUpdated = await _sociosService.Update(socio);
                return Ok(socioUpdated);
            }
            catch (Exception)
            {
                if (!SocioExists(id))
                    return NotFound();
                throw;
            }
        }


        /// <summary>
        /// Insertaremos un nuevo socio
        /// </summary>
        /// <param name="socio"></param>
        /// <returns></returns>
        // POST: api/Socios
        [HttpPost]
        public async Task<ActionResult<Socio>> PostSocio(VMSocio socio)
        {
            try
            {
                var socioOk = await _sociosService.Add(socio);
                return Ok(socioOk);
            }
            catch (DbUpdateException)
            {
                if (SocioExists(socio.Id))
                    return Conflict();
                throw;
            }
        }


        /// <summary>
        /// Borrado del socio indicado
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE: api/Socios/5
        [HttpDelete("{id:long}")]
        public async Task<ActionResult<Socio>> DeleteSocio(long id)
        {
            try
            {
                if (await _sociosService.Delete(id))
                    return Ok();
            }
            catch (Exception)
            {
                if (!SocioExists(id))
                    return NotFound();
                throw;
            }

            return BadRequest("Se ha producido un error");
        }

        private bool SocioExists(long id)
        {
            return _sociosService.GetById(id) != null;
        }
    }
}